<?php

namespace studiocreativateam\Models;

use SilverStripe\Assets\Image;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Permission;

class SortedImage extends DataObject
{
    private static $table_name = 'SortedImage';

    private static $db = [
        'SortOrder' => 'Int',
    ];

    private static $owns = ['Image'];
    private static $has_one = [
        'Image' => Image::class,
    ];

    private static $default_sort = 'SortOrder';

    private static $summary_fields = [
        'Thumbnail'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName('SortOrder');
        return $fields;
    }

    public function getThumbnail()
    {
        return $this->Image()->exists() ? $this->Image()->Fill(80, 80) : false;
    }

    public function canView($member = null, $context = array())
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null, $context = array())
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null, $context = array())
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = array())
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }
}